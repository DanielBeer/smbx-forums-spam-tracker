import urllib
from urllib.request import urlopen
from bs4 import BeautifulSoup
import re
import discord
import discord.utils
from discord.ext import commands
import datetime
import random
import asyncio


GAME_TEXT = ('catch a duck', 'in my bucket', 'with the fishes', 'science', 'in the mill', 'Cartesian Dualism', 'coconut migration')


TOKEN = 'NTM2NDIwNDE5NzgwMjE0ODEy.DyWfcQ.Y2LJuQRxynpDYG3I4-pTyJdUkPs'
LOOP_DELAY_TIME = 20


bot = commands.Bot(command_prefix='-!')


def load_list(filename):
	return [word.rstrip() for word in open('Word Lists/' + filename + '.txt').readlines()]


SAFE_SITES = load_list('safe-sites')
BL_WORDS = load_list('blacklisted-words')
SAFE_DOM_EXTS = load_list('safe-dom-exts')
IMG_EXTS = load_list('img-exts')


def safe(link):
	for site in SAFE_SITES:
		if site in link:
			return True
	
	return False
	
	
ping = False
	
	
async def send_update(name, rating, url, log_to_file=True, update_text='A new user has registered'):
	open('log.txt', 'a').close()

	with open('log.txt', 'r+') as log_file:
		lines = log_file.readlines()
	
	
		msg = update_text + ': ' + name + '\n' + 'Spam rating: ' + str(rating) + '%\n'

		if rating == 0:
			msg += 'NO RISK\n'
		elif rating < 30:
			msg += 'LOW RISK\n'
		elif rating < 50:
			msg += 'MODERATE RISK\n'
			
			if ping:
				msg += '@staff\n'
		else:
			msg += 'HIGH RISK\n'
			
			if ping:
				msg += '@staff\n'
			
		msg += 'Profile: ' + url
		
		for guild in bot.guilds:
			await discord.utils.get(guild.channels, name='spam-tracker').send(msg)
			
		msg = re.search(r'[0-9]+$', url).group(0) + '\n' + str(datetime.datetime.now()) + '\n' + msg
		
		log_file.seek(0)
		
		print(lines)
		
		if len(lines) > 0:
			msg += '\n------------------------------\n'
			
		lines = [msg] + lines
		
		print(lines)
		
		log_file.writelines(lines)


dir_html = None
member_list = 'https://www.supermariobrosx.org/forums/memberlist.php?sk=c&sd=d&username=&search_group_id=0&joined_select=lt&count_select=eq&joined=&count='


async def refresh_dir_page():
	global dir_html

	while True:
		try: 
			dir_page = urlopen(member_list).read()
			dir_html = BeautifulSoup(dir_page, 'lxml').prettify()
			
			break
		except urllib.error.HTTPError:
			await asyncio.sleep(1)


def check_blacklisted(text):
	num = 0

	for word in BL_WORDS:
		p = re.compile(re.compile('[^a-z]' + word + 's*[^a-z]'))
	
		while True:
			found = re.search(p, text)
			
			if found:
				found = found.group(0)
				
				print(found)
				
				text = text.replace(found, ' ')
				num += 1
			else:
				break
				
	return num
	
	
def check_dom_exts(links):
	num = 0

	for link in links:
		print(link)
		ext = re.search(r'\.[a-zA-Z]+/*', link)
		
		print(ext.group(0)[1:-1])
		
		if ext and not ext.group(0)[1:-1] in SAFE_DOM_EXTS:
			num += 1
			
	return num


def get_links(text):
	links = []
	
	print(text)

	while True:
		link = re.search(r'^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$', text)
		
		if link:
			link = link.group(0)
		
			text = text.replace(link, ' ')
			
			img_ext = re.search(r'\.[a-zA-Z0-9]{2,4}$', link)
			
			if not safe(link) and (not img_ext or not img_ext.group(0)[1:] in IMG_EXTS):
				links.append(link)
		else:
			break
			
	return links
	
	
def strip_cat(pattern, text):
	print(pattern)

	return re.sub('\s+', ' ', re.sub(pattern, '', text))
	
	
async def log_user_eval(profile_url, log_to_file=True):
	spam_chance = 0
	page = None
	
	while True:	
		try:
			page = urlopen(profile_url)
			
			break
		except urllib.error.HTTPError:
			await asyncio.sleep(1)
	
	profile_html = BeautifulSoup(page, 'lxml').prettify()

	name = re.search(r'Viewing profile\s-\s[^\n\r]+', profile_html)

	if name:
		name = re.sub(r'Viewing profile\s-\s', '', name.group(0))
		
	flair = re.search(r'Flair:\s*\</dt\>\s*\<dd\>\s*[^\n\r]*', profile_html)

	if flair:
		flair = re.sub(r'Flair:\s*\</dt\>\s*\<dd\>\s*', '', flair.group(0))
		
	chat_name = re.search(r'Chat\sUsername:\s*</dt\>\s*\<dd\>\s*[^\n\r]*?\s*\</dd\>', profile_html)

	if chat_name:
		chat_name = re.sub(r'Chat\sUsername:\s*</dt\>\s*\<dd\>\s*|\</dd\>', '', chat_name.group(0))

	projects = re.search(r'Project:\s*\</dt\>\s*\<dd\>[\s\S]+?\</dd\>', profile_html)

	if projects:
		projects = strip_cat(r'Project:\s*\</dt\>\s*\<dd\>|\</dd\>', projects.group(0))
		
		if len(projects) > 150:
			spam_chance += (len(projects) - 150) * 0.06
		
	interests = re.search(r'Interests:\s*\</dt\>\s*\<dd\>[\s\S]+?\</dd\>', profile_html)

	if interests:
		interests = strip_cat(r'Interests:\s*\</dt\>\s*\<dd\>|\</dd\>', interests.group(0))
		
		if len(interests) > 200:
			spam_chance += (len(interests) - 200) * 0.04
		
	occupation = re.search(r'Occupation:\s*\</dt\>\s*\<dd\>[\s\S]+?\</dd\>', profile_html)

	if occupation:
		occupation = strip_cat(r'Occupation:\s*\</dt\>\s*\<dd\>|\</dd\>', occupation.group(0))
		
		if len(occupation) > 100:
			spam_chance += (len(occupation) - 100) * 0.08
	
	print('cp0')
	
	contacts = re.search(r'\<h3\>\s*Contact[\s\S]+?\</dl\>', profile_html)

	if contacts:
		contacts = strip_cat(r'\<h3\>\s*Contact|\</dl\>', contacts.group(0))
	
	print('cp1')
	
	signature = re.search(r'\<div\sclass="signature\sstandalone"\>[\s\S]+?\</table\>', profile_html)

	if signature:
		signature = strip_cat(r'\<div\sclass="signature\sstandalone"\>|\</table\>', signature.group(0))

	print('cp2')
	
	user_text = ''
	
	for text in (name, flair, chat_name, projects, interests, occupation, contacts, signature):
		if text:
			user_text += text + ' '

	links = get_links(user_text)
	
	print(links)
	
	spam_chance += check_dom_exts(links) * 10
	
	print('cp3')
	
	if signature:
		signature = re.sub(r'href="[\s\S]+?"\>', '', signature)
		
		if len(signature) < 550 and len(links) > 0:
			spam_chance += 20
	elif not signature:
		spam_chance += 5

	for link in links:
		if name in link or (chat_name and chat_name in link):
			spam_chance += 35
		elif 'bit.ly' in link or 'tinyurl' in link:
			spam_chance += 40
		else:
			spam_chance += 25
			
	print('cp3')

	group = re.search(r'Groups:\s*\</dt\>\s*\<dd\>\s*\<select\sname="[a-z]"\>\s*\<option\sselected="selected"\svalue="[0-9]"\>[\s\S]+?\<', profile_html)
	group = re.sub(r'Groups:\s*\</dt\>\s*\<dd\>\s*\<select\sname="[a-z]"\>\s*\<option\sselected="selected"\svalue="[0-9]"\>|\s+', '', group.group(0)[:-1])
	
	print('cp4')
	
	user_text = user_text.lower()
	
	spam_chance += check_blacklisted(profile_html) * 25

	spam_chance = min(max(0, spam_chance), 100)
	
	print('cp5')

	if group != 'Registeredusers':
		spam_chance = 0
		
	print('cp6')
	
	await send_update(name, spam_chance, profile_url, log_to_file=log_to_file)
	
	print('cp7')
	
	return spam_chance, name

	
def get_url(id):
	return 'https://www.supermariobrosx.org/forums/memberlist.php?mode=viewprofile&u=' + str(id)	
	
	
async def get_id(recent_id, id, behind):
	print('BEHIND CALL? ' + str(behind))
	
	while True:	
		if (recent_id == id and not behind) or recent_id < id:
			return id
		elif id == -1:
			id = recent_id
		
		try:
			urlopen(get_url(id))

			return id
		except urllib.error.URLError:
			id += 1
		except urllib.error.HTTPError:
			await asyncio.sleep(1)


async def get_recent():
	await refresh_dir_page()

	recent_profile_url = 'https://www.supermariobrosx.org/forums/' + re.sub(r'amp;', '', re.search(r'memberlist\.php\?mode=viewprofile\&amp;u=[0-9]{1,5}', dir_html).group(0))
	
	return int(re.sub('\D', '', recent_profile_url))
	

@bot.event
async def on_ready():
	await bot.change_presence(activity=discord.Game(name=random.SystemRandom().choice(GAME_TEXT)))

	print('READY\n---------------------------')
	
	recent_id = await get_recent()
	id = 17390
	is_behind = False
	
	try:
		with open('log.txt', 'r') as log_file:
			line = log_file.readline().rstrip('\n')
	
			if line != '':
				id = int(line) + 1
	except FileNotFoundError:
		pass

	while True:
		print('MOST RECENT: ' + str(recent_id))
		print('CHECKING: ' + str(id))
		
		id = await get_id(recent_id, id, is_behind)
		
		is_behind = id <= recent_id
		
		if is_behind:
			print(id)
		
			rating, _ = await log_user_eval(get_url(id), log_to_file=True)
			
			if rating < 30:
				with open('watchlist.txt', 'a') as wl_file:
					now = datetime.datetime.now()
				
					wl_file.write(str(id) + ' ' + str(rating) + ' ' + str(now.hour) + ':' + str(now.minute - 1) + '\n')
			
			id += 1
		
		print('BEHIND LOOP? ' + str(is_behind))
		
		if not is_behind:
			print('GOING TO SLEEP')
		
			await asyncio.sleep(LOOP_DELAY_TIME)
			
			recent_id = await get_recent()
			
			if recent_id > id:
				is_behind = True
				id += 1
			
			now = datetime.datetime.now()
			
			current_hour, current_minute = now.hour, now.minute
			
			if current_minute == 0 and now.second < LOOP_DELAY_TIME:
				await bot.change_presence(activity=discord.Game(name=random.SystemRandom().choice(GAME_TEXT)))
				
			try:
				with open('watchlist.txt', 'r+') as wl_file:
					lines = wl_file.readlines()
				
					for i in range(len(lines)):
						user = lines[i].split(' ')
						id = int(user[0])
						rating = float(user[1])
						time = user[2].split(':')
						hour = int(time[0])
						minute = int(time[1])
						
						if hour == current_hour and minute == current_minute:
							del lines[i]
						else:
							url = get_url(id)
						
							while True:
								try:
									urlopen(url)
						
									new_rating, name = await log_user_eval(url, log_to_file=False)
									
									if new_rating > 30 and new_rating > rating:
										await send_update(name, new_rating, url, log_to_file=False, update_text='A watched user has updated their profile')
									
									break
								except urllib.error.URLError:
									break
								except urllib.error.HTTPError:
									await asyncio.sleep(1)
									
			except FileNotFoundError:
				pass


@bot.command()
@commands.has_permissions(manage_guild=True)
async def evaluser(ctx, id):
	url = get_url(id)

	while True:
		try:
			urlopen(url)
			
			await log_user_eval(url, log_to_file=False)
			
			break
		except urllib.error.URLError:
			for guild in bot.guilds:
				await discord.utils.get(guild.channels, name='spam-tracker').send('ID not in range or the requested account has been deleted')
				
			break
		except urllib.error.HTTPError:
			await asyncio.sleep(1)


print('CONNECTING...')
bot.run(TOKEN)